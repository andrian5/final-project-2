<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegenerateOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'name' => 'required',
            'email' => 'required|unique:users,email|email',
            'username' => 'required|unique:users,username',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $user = User: :where('email' , $request->email)->first();

        dd($user->otp_code);
        
        $user->otp_code()->delete();
        //
    }
}
