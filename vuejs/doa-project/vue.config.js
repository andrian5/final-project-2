module.exports = {
  devServer: {
    disableHostCheck: true,
    port: 8081,
    // public: ‘0.0.0.0:8080’
},
  publicPath: "/",
  
  transpileDependencies: [
    'vuetify'
  ]
}
