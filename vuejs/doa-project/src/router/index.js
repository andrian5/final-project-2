import Vue from 'vue'
import VueRouter from 'vue-router'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    path: '/prays',
    name: 'Prays',
    component: () => import(/* webpackChunkName: "prays" */ '../views/Prays.vue')
  },
  {
    path: '/pray/:id',
    name: 'Pray',    
    component: () => import(/* webpackChunkName: "about" */ '../views/Pray.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
