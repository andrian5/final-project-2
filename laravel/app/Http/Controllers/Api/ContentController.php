<?php

namespace App\Http\Controllers\Api;

use App\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ContentResource;
use Illuminate\Support\Facades\Validator;

class ContentController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store' , 'update' , 'destroy']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contents = ContentResource::collection(Content::latest()->get());

        return response()->json([
            'success' => true,
            'message' => 'Data daftar Content berhasil ditampilkan',
            'data'    => $contents
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
        
        $validator = Validator::make($allRequest , [
            'nama_doa' => 'required',
            'lafazh' => 'required',
            'latin' => 'required',
            'artinya' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors() , 400);
        }

        $content = Content::create([
            'nama_doa' => $request->nama_doa,
            'lafazh' => $request->lafazh,
            'latin' => $request->latin,
            'artinya' => $request->artinya,
        ]);

        if($content){
            return response()->json([
                'success'   => true,
                'message'   => 'Data Content berhasil dibuat',
                'data'      =>  $content
            ], 200);
        }

        return response()->json([
            'success'   => false,
            'message'   => 'Data Content gagal dibuat'
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $content = Content::find($id);

        if($content)
        {
            return response()->json([
                'success' => true,
                'message' => 'Data Content berhasil ditampilkan',
                'data'    => $content
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Content dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Content $content)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'nama_doa' => 'required',
            'lafazh' => 'required',
            'latin' => 'required',
            'artinya' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $content = Content::find($id);

        if($content)
        {
            /* $user = auth()->user();

            if($content->user_id != $user->id)
            {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Content bukan milik user login',
                ] , 403);

            } */

            $content->update([
                'nama_doa' => $request->nama_doa,
                'lafazh' => $request->lafazh,
                'latin' => $request->latin,
                'artinya' => $request->artinya,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Data dengan Nama Doa : ' . $content->nama_doa . '  berhasil diupdate',
                'data' =>    $content
            ]);
        }

        return response()->json([
            'success' => false,
            'message' => 'Content dengan id : '. $id .' tidak ditemukan',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $content = Content::find($id);

        if ($content) {
           /* $user = auth()->user();

            if ($content->user_id != $user->id) {
                return response()->json([
                    'success' => false,
                    'message' => 'Data Content bukan milik user login',
                ], 403);
            } */

            $content->delete();

            return response()->json([
                'success' => true,
                'message' => 'Data Content berhasil dihapus',
            ], 200); 
        } 

        return response()->json([
            'success' => false,
            'message' => 'Content dengan id : ' .  $id . '  tidak ditemukan',
        ], 404);
    }
}
