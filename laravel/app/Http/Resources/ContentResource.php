<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        => $this->id,
            'nama_doa'  => $this->nama_doa,
            'lafazh'    => $this->lafazh,
            'latin'     => $this->latin,
            'artinya'   => $this->artinya,
        ];
    }
}
